﻿using System;

namespace Binance.MarketAnalyzer.Models
{
    public class CandlestickResult
    {
        public string Name { get; set; }

        public Uri HelpLink { get; set; }

        public int Score { get; set; }
    }
}
