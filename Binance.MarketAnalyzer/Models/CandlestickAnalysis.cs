﻿using Binance.MarketAnalyzer.Service;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Binance.MarketAnalyzer.Models
{
    public class CandlestickAnalysis
    {
        public string Symbol { get; set; }

        public string BaseAsset { get; set; }

        public string QuoteAsset { get; set; }

        public Uri BinanceLink
        {
            get
            {
                return new Uri($"https://www.binance.com/en/trade/pro/{BaseAsset}_{QuoteAsset}");
            }
        }

        public int Score
        {
            get
            {
                return Patterns.Select(x => x.Points).Sum();
            }
        }

        public List<ICandlestickPattern> Patterns { get; set; } = new List<ICandlestickPattern>();
    }
}
