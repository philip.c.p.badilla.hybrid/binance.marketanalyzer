﻿using Binance.MarketAnalyzer.Models;
using Binance.MarketAnalyzer.Service;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Binance.MarketAnalyzer.Controllers
{
    public class HomeController : Controller
    {
        private CandlestickAnalyzerService candlestickAnalyzer = new CandlestickAnalyzerService
        {
            QuoteAsset = "BTC",
            Interval = Net.Objects.KlineInterval.TwelfHour
        };

        public HomeController()
        {

        }

        public async Task<IActionResult> Index()
        {

            //var result1 = await candlestickAnalyzer.Analyze(new Net.Objects.BinanceSymbol
            //{
            //    BaseAsset = "DOCK",
            //    QuoteAsset = "BTC",
            //    Name = "DOCKBTC"
            //});

            var results = await candlestickAnalyzer.AnalyzeAll();

            return View(results);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
