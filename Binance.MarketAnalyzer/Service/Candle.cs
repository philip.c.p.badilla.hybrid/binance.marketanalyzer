﻿using Binance.Net.Objects;

namespace Binance.MarketAnalyzer.Service
{
    /// <summary>
    /// https://stockcharts.com/school/doku.php?id=chart_school:chart_analysis:introduction_to_candlesticks
    /// </summary>
    public enum CandleType
    {
        Unknown,
        Doji,
        DragonFlyDoji,
        GraveStoneDoji,
        Long,
        Short,
        Marobuzu,
        Pin,
        ReversePin,
        SpiningTop
    }

    public class Candle
    {
        public bool IsGreen { get; private set; }

        public bool IsRed { get; private set; }

        public decimal Open { get; set; }

        public decimal High { get; set; }

        public decimal Low { get; set; }

        public decimal Close { get; set; }

        public decimal Volume { get; set; }
        
        public int TradeCount { get; set; }

        public decimal Height { get; private set; }

        public decimal BodyHeight { get; private set; }

        public decimal BodyRatio { get; private set; }

        public CandleType Type { get; private set; }

        public Candle(BinanceKline kline)
        {
            Open = kline.Open;
            High = kline.High;
            Low = kline.Low;
            Close = kline.Close;
            Volume = kline.Volume;
            TradeCount = kline.TradeCount;

            Height = kline.High - kline.Low;
            BodyHeight = kline.Open - kline.Close;
            BodyHeight = BodyHeight < 0 ? BodyHeight * -1 : BodyHeight;
            BodyRatio = BodyHeight / Height;
            IsGreen = Close >= Open;
            IsRed = Close < Open;
            Type = GetCandleType();
        }

        private CandleType GetCandleType()
        {
            // Marubozu
            if (BodyRatio == 1m) return CandleType.Marobuzu;

            // Regular
            if (BodyRatio >= .5m) return CandleType.Long;
            if (BodyRatio >= .3m) return CandleType.Short;

            // Doji
            if (BodyRatio <= .1m)
            {
                if (Open == High && Open == Close) return CandleType.DragonFlyDoji;
                else if (Open == Low && Open == Close) return CandleType.GraveStoneDoji;
                else return CandleType.Doji;
            }

            // Pin
            if (BodyRatio <= .25m)
            {
                if (BodyOnBottom) return CandleType.ReversePin;
                else if (BodyOnTop) return CandleType.Pin;
                else return CandleType.SpiningTop;
            }

            return CandleType.Unknown;
        }

        public bool BodyOnBottom
        {
            get
            {
                decimal factor = .33m;
                var lowerCandleLimit = Low + ((High - Low) * factor);
                return Open <= lowerCandleLimit && Close <= lowerCandleLimit;
            }
        }

        public bool BodyOnTop
        {
            get
            {
                decimal factor = .66m;
                var upperCandleLimit = Low + ((High - Low) * factor);
                return Open >= upperCandleLimit && Close >= upperCandleLimit;
            }
        }
    }
}
