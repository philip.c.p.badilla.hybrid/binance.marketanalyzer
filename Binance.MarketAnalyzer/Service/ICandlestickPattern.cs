﻿using Binance.Net.Objects;
using System;

namespace Binance.MarketAnalyzer.Service
{
    public enum CandlestickPatternType
    {
        Bullish,
        Bearish
    }

    public interface ICandlestickPattern
    {
        string Name { get; }

        Uri HelpLink { get; }

        string Remarks { get; }

        int Points { get; }

        CandlestickPatternType Type { get; }

        bool Analyze(BinanceKline[] klines);
    }
}
