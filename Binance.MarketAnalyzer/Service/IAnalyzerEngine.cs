﻿using Binance.Net.Objects;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Binance.MarketAnalyzer.Service
{
    public interface IAnalyzerEngine<T>
    {
        Task<List<T>> AnalyzeAll();

        Task<T> Analyze(BinanceSymbol symbol);
    }
}
