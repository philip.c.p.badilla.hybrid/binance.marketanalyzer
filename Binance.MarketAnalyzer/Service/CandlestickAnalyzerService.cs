﻿using Binance.MarketAnalyzer.Models;
using Binance.MarketAnalyzer.Service.CandlestickPatterns;
using Binance.Net;
using Binance.Net.Objects;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Binance.MarketAnalyzer.Service
{
    public class CandlestickAnalyzerService : IAnalyzerEngine<CandlestickAnalysis>
    {
        BinanceClient client = new BinanceClient();

        public KlineInterval Interval { get; set; } = KlineInterval.OneDay;

        public string QuoteAsset { get; set; } = "BTC";

        public CandlestickAnalyzerService(string baseAsset = "BTC", KlineInterval interval = KlineInterval.OneDay)
        {
            QuoteAsset = baseAsset;
            Interval = interval;
        }

        public CandlestickAnalyzerService(KlineInterval interval)
        {
            Interval = interval;
        }

        public async Task<List<CandlestickAnalysis>> AnalyzeAll()
        {
            var results = new List<CandlestickAnalysis>();

            var exchangeInfo = await client.GetExchangeInfoAsync();

            if (exchangeInfo.Success)
            {
                foreach (var symbol in exchangeInfo.Data.Symbols)
                {
                    if (symbol.Status == SymbolStatus.Trading && symbol.QuoteAsset == QuoteAsset)
                    {
                        var result = await Analyze(symbol);
                        if (result.Patterns.Count > 0)
                        {
                            results.Add(result);
                        }
                    }
                }
            }

            client.Dispose();

            return results;
        }

        public async Task<CandlestickAnalysis> Analyze(BinanceSymbol symbol)
        {
            var result = new CandlestickAnalysis
            {
                Symbol = symbol.Name,
                BaseAsset = symbol.BaseAsset,
                QuoteAsset = symbol.QuoteAsset,
            };           
            var klines = await client.GetKlinesAsync(symbol.Name, Interval, limit: 10);

            if (klines.Success)
            {
                var patterns = AnalyzeCandlestickPatterns(klines.Data);                
                result.Patterns = patterns;
            }

            return result;
        }

        private List<ICandlestickPattern> AnalyzeCandlestickPatterns(BinanceKline[] klines)
        {
            var results = new List<ICandlestickPattern>();

            // Bullish
            var bullishPinBar = new BullishPinBar();
            if (bullishPinBar.Analyze(klines)) results.Add(bullishPinBar);

            var tweezerBottom = new TweezerBottom();
            if (tweezerBottom.Analyze(klines)) results.Add(tweezerBottom);

            var oneWhiteSoldier = new OneWhiteSoldier();
            if (oneWhiteSoldier.Analyze(klines)) results.Add(oneWhiteSoldier);

            var morningStar = new MorningStar();
            if (morningStar.Analyze(klines)) results.Add(morningStar);

            // Bearish
            var bearishPinBar = new BearishPinBar();
            if (bearishPinBar.Analyze(klines)) results.Add(bearishPinBar);

            var tweezerTop = new TweezerTop();
            if (tweezerTop.Analyze(klines)) results.Add(tweezerTop);

            var oneBlackCrow = new OneBlackCrow();
            if (oneBlackCrow.Analyze(klines)) results.Add(oneBlackCrow);

            var eveningStar = new EveningStar();
            if (eveningStar.Analyze(klines)) results.Add(eveningStar);

            return results;
        }
    }
}
