﻿using Binance.Net.Objects;
using System;


namespace Binance.MarketAnalyzer.Service.CandlestickPatterns
{
    public class BearishEngulfing : ICandlestickPattern
    {
        public string Name => "Bearish Engulfing";

        public CandlestickPatternType Type => CandlestickPatternType.Bearish;

        public Uri HelpLink => new Uri("https://www.youtube.com/watch?v=hoGkfzJeR6A&t=2701s");

        public string Remarks => "Must appear on a resistance line in an downtrend or uptrend if overbought.";

        public int Points => 150;

        public bool Analyze(BinanceKline[] klines)
        {
            var candle1 = new Candle(klines[klines.Length - 4]);
            var candle2 = new Candle(klines[klines.Length - 3]);
            var candle3 = new Candle(klines[klines.Length - 2]);
            var candle4 = new Candle(klines[klines.Length - 1]);

            bool isEngulfing = candle4.IsRed && candle4.BodyHeight > candle3.BodyHeight && candle4.Close < candle3.Open;

            return candle1.IsGreen && candle2.IsGreen && candle3.IsGreen && isEngulfing;
        }
    }
}
