﻿using Binance.Net.Objects;
using System;

namespace Binance.MarketAnalyzer.Service.CandlestickPatterns
{
    public class TweezerBottom : ICandlestickPattern
    {
        public string Name => "Tweezer Bottom";

        public CandlestickPatternType Type => CandlestickPatternType.Bullish;

        public Uri HelpLink => new Uri("https://www.youtube.com/watch?v=hoGkfzJeR6A&t=2701s");

        public string Remarks => "Must appear on the support line.";

        public int Points => 100;

        public bool Analyze(BinanceKline[] klines)
        {
            var latestCandle = new Candle(klines[klines.Length - 1]);
            var previousCandle = new Candle(klines[klines.Length - 2]);

            bool latestCandleHasCorrectType = (latestCandle.Type == CandleType.Long || latestCandle.Type == CandleType.Marobuzu) 
                && latestCandle.IsGreen 
                && (latestCandle.BodyHeight >= previousCandle.BodyHeight * .5m)
                && (latestCandle.Close > previousCandle.Close);
            bool previousCandleHasCorrectType = (previousCandle.Type == CandleType.Long || previousCandle.Type == CandleType.Marobuzu)
                && previousCandle.IsRed;

            return latestCandleHasCorrectType && previousCandleHasCorrectType;
        }
    }
}
