﻿using Binance.Net.Objects;
using System;

namespace Binance.MarketAnalyzer.Service.CandlestickPatterns
{
    public class OneWhiteSoldier : ICandlestickPattern
    {
        public string Name => "One White Soldier";

        public CandlestickPatternType Type => CandlestickPatternType.Bullish;

        public Uri HelpLink => new Uri("https://www.youtube.com/watch?v=hoGkfzJeR6A&t=2701s");

        public string Remarks => "Must appear on the support of an up trend.";

        public int Points => 100;

        public bool Analyze(BinanceKline[] klines)
        {
            var latestCandle = new Candle(klines[klines.Length - 1]);
            var previousCandle = new Candle(klines[klines.Length - 2]);

            bool latestCandleHasCorrectType = (latestCandle.Type == CandleType.Long || latestCandle.Type == CandleType.Marobuzu) && latestCandle.IsGreen;
            bool previousCandleHasCorrectType = (previousCandle.Type == CandleType.Long || previousCandle.Type == CandleType.Marobuzu) && previousCandle.IsRed;
            bool positionIsCorrect = latestCandle.Open > previousCandle.Close && latestCandle.Close > previousCandle.Open;

            return latestCandleHasCorrectType && previousCandleHasCorrectType && positionIsCorrect;
        }
    }
}
