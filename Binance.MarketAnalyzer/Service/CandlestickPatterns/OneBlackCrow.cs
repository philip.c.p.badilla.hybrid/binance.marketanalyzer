﻿using Binance.Net.Objects;
using System;

namespace Binance.MarketAnalyzer.Service.CandlestickPatterns
{
    public class OneBlackCrow : ICandlestickPattern
    {
        public string Name => "One Black Crow";

        public CandlestickPatternType Type => CandlestickPatternType.Bearish;

        public Uri HelpLink => new Uri("https://www.youtube.com/watch?v=hoGkfzJeR6A&t=2701s");

        public string Remarks => "Must appear on the resistance of a down trend.";

        public int Points => -200;

        public bool Analyze(BinanceKline[] klines)
        {
            var latestCandle = new Candle(klines[klines.Length - 1]);
            var previousCandle = new Candle(klines[klines.Length - 2]);

            bool latestCandleHasCorrectType = (latestCandle.Type == CandleType.Long || latestCandle.Type == CandleType.Marobuzu) && latestCandle.IsRed;
            bool previousCandleHasCorrectType = (previousCandle.Type == CandleType.Long || previousCandle.Type == CandleType.Marobuzu) && previousCandle.IsGreen;
            bool positionIsCorrect = latestCandle.Open < previousCandle.Close && latestCandle.Close < previousCandle.Open;

            return latestCandleHasCorrectType && previousCandleHasCorrectType && positionIsCorrect;
        }
    }
}
