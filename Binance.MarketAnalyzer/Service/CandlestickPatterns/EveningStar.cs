﻿using Binance.Net.Objects;
using System;

namespace Binance.MarketAnalyzer.Service.CandlestickPatterns
{
    public class EveningStar : ICandlestickPattern
    {
        public string Name => "Evening Star";

        public CandlestickPatternType Type => CandlestickPatternType.Bearish;

        public Uri HelpLink => new Uri("https://www.youtube.com/watch?v=hoGkfzJeR6A&t=2701s");

        public string Remarks => "Must appear on the support of an up trend.";

        public int Points => -150;

        public bool Analyze(BinanceKline[] klines)
        {
            var candle1 = new Candle(klines[klines.Length - 5]);
            var candle2 = new Candle(klines[klines.Length - 4]);
            var candle3 = new Candle(klines[klines.Length - 3]);
            var candle4 = new Candle(klines[klines.Length - 2]);
            var candle5 = new Candle(klines[klines.Length - 1]);

            bool candle1IsCorrect = (candle1.Type == CandleType.Long || candle1.Type == CandleType.Marobuzu) && candle1.IsGreen;
            bool candle2IsCorrect = (candle2.Type == CandleType.Long || candle2.Type == CandleType.Marobuzu) && candle2.IsGreen;
            bool candle3IsCorrect = (candle3.Type == CandleType.Long || candle3.Type == CandleType.Marobuzu) && candle3.IsGreen;
            bool candle4IsCorrect = candle4.Type == CandleType.Short || candle4.Type == CandleType.Doji || candle4.Type == CandleType.DragonFlyDoji;
            bool candle5IsCorrect = (candle5.Type == CandleType.Long || candle5.Type == CandleType.Marobuzu) && candle5.IsRed;

            return (candle1IsCorrect && candle2IsCorrect && candle3IsCorrect && candle4IsCorrect && candle5IsCorrect);
        }
    }
}
