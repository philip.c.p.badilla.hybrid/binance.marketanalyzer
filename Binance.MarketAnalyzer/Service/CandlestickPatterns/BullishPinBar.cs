﻿using Binance.Net.Objects;
using System;

namespace Binance.MarketAnalyzer.Service.CandlestickPatterns
{
    public class BullishPinBar : ICandlestickPattern
    {
        public string Name => "Bullish Pin Bar";

        public CandlestickPatternType Type => CandlestickPatternType.Bullish;

        public Uri HelpLink => new Uri("https://www.youtube.com/watch?v=hoGkfzJeR6A&t=2701s");

        public string Remarks => "Must appear on the support of an up trend.";

        public int Points => 100;

        public bool Analyze(BinanceKline[] klines)
        {
            var candle = new Candle(klines[klines.Length - 1]);

            return candle.Type == CandleType.Pin || candle.Type == CandleType.DragonFlyDoji;
        }
    }
}
