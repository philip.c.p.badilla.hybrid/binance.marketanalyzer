﻿using Binance.Net.Objects;
using System;


namespace Binance.MarketAnalyzer.Service.CandlestickPatterns
{
    public class BullishEngulfing : ICandlestickPattern
    {
        public string Name => "Bullish Engulfing";

        public CandlestickPatternType Type => CandlestickPatternType.Bullish;

        public Uri HelpLink => new Uri("https://www.youtube.com/watch?v=hoGkfzJeR6A&t=2701s");

        public string Remarks => "Must appear on a support line in an uptrend or downtrend if oversold.";

        public int Points => 150;

        public bool Analyze(BinanceKline[] klines)
        {
            var candle1 = new Candle(klines[klines.Length - 4]);
            var candle2 = new Candle(klines[klines.Length - 3]);
            var candle3 = new Candle(klines[klines.Length - 2]);
            var candle4 = new Candle(klines[klines.Length - 1]);

            bool isEngulfing = candle4.IsGreen && candle4.BodyHeight > candle3.BodyHeight && candle4.Close > candle3.Open;

            return candle1.IsRed && candle2.IsRed && candle3.IsRed && isEngulfing;
        }
    }
}
