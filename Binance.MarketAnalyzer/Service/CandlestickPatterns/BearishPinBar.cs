﻿using Binance.Net.Objects;
using System;

namespace Binance.MarketAnalyzer.Service.CandlestickPatterns
{
    public class BearishPinBar : ICandlestickPattern
    {
        public string Name => "Bearish Pin Bar";

        public CandlestickPatternType Type => CandlestickPatternType.Bearish;

        public Uri HelpLink => new Uri("https://www.youtube.com/watch?v=hoGkfzJeR6A&t=2701s");

        public string Remarks => "Must appear on the resistance of a down trend.";

        public int Points => -100;

        public bool Analyze(BinanceKline[] klines)
        {
            var candle = new Candle(klines[klines.Length - 1]);

            return candle.Type == CandleType.ReversePin || candle.Type == CandleType.GraveStoneDoji;
        }
    }
}
