﻿using Binance.MarketAnalyzer.Service.CandlestickPatterns;
using Binance.Net.Objects;
using FluentAssertions;
using Xunit;

namespace Binance.MarketAnalyzer.UnitTests
{
    public class CandleStickPatternTests
    {
        // Bearish Pin Bar
        [Fact]
        public void BearishPinBar_WhenCandleIsReversePinGreen_ShouldReturnTrue()
        {
            // Arrange
            var analyzer = new BearishPinBar();
            var klines = new BinanceKline[] 
            {
                new BinanceKline
                {
                    High = 100,
                    Close = 30,
                    Open = 5,
                    Low = 0
                }
            };

            var result = analyzer.Analyze(klines);

            result.Should().BeTrue();
        }

        [Fact]
        public void BearishPinBar_WhenCandleIsReversePinRed_ShouldReturnTrue()
        {
            // Arrange
            var analyzer = new BearishPinBar();
            var klines = new BinanceKline[]
            {
                new BinanceKline
                {
                    High = 100,
                    Close = 5,
                    Open = 30,
                    Low = 0
                }
            };

            var result = analyzer.Analyze(klines);

            result.Should().BeTrue();
        }

        [Fact]
        public void BearishPinBar_WhenCandleIsGraveStoneDoji_ShouldReturnTrue()
        {
            // Arrange
            var analyzer = new BearishPinBar();
            var klines = new BinanceKline[]
            {
                new BinanceKline
                {
                    High = 100,
                    Close = 0,
                    Open = 0,
                    Low = 0
                }
            };

            var result = analyzer.Analyze(klines);

            result.Should().BeTrue();
        }

        // Bullish Pin Bar
        [Fact]
        public void BullishPinBar_WhenCandleIsReversePinGreen_ShouldReturnTrue()
        {
            // Arrange
            var analyzer = new BullishPinBar();
            var klines = new BinanceKline[]
            {
                new BinanceKline
                {
                    High = 100,
                    Close = 95,
                    Open = 70,
                    Low = 0
                }
            };

            var result = analyzer.Analyze(klines);

            result.Should().BeTrue();
        }

        [Fact]
        public void BullishPinBar_WhenCandleIsReversePinRed_ShouldReturnTrue()
        {
            // Arrange
            var analyzer = new BullishPinBar();
            var klines = new BinanceKline[]
            {
                new BinanceKline
                {
                    High = 100,
                    Close = 70,
                    Open = 95,
                    Low = 0
                }
            };

            var result = analyzer.Analyze(klines);

            result.Should().BeTrue();
        }

        [Fact]
        public void BullishPinBar_WhenCandleIsPin_ShouldReturnTrue()
        {
            // Arrange
            var analyzer = new BullishPinBar();
            var klines = new BinanceKline[]
            {
                new BinanceKline
                {
                    High = 100,
                    Close = 100,
                    Open = 100,
                    Low = 0
                }
            };

            var result = analyzer.Analyze(klines);

            result.Should().BeTrue();
        }

        // Tweezer Bottom
        [Fact]
        public void TweezerBottom_WhenCandleIsMarobuzu_ShouldReturnTrue()
        {
            // Arrange
            var analyzer = new TweezerBottom();
            var klines = new BinanceKline[]
            {
                new BinanceKline
                {
                    High = 100,
                    Close = 0,
                    Open = 100,
                    Low = 0
                },
                new BinanceKline
                {
                    High = 100,
                    Close = 100,
                    Open = 0,
                    Low = 0
                }
            };

            var result = analyzer.Analyze(klines);

            result.Should().BeTrue();
        }

        [Fact]
        public void TweezerBottom_WhenCandleIsLong_ShouldReturnTrue()
        {
            // Arrange
            var analyzer = new TweezerBottom();
            var klines = new BinanceKline[]
            {
                new BinanceKline
                {
                    High = 100,
                    Close = 20,
                    Open = 80,
                    Low = 0
                },
                new BinanceKline
                {
                    High = 100,
                    Close = 80,
                    Open = 20,
                    Low = 0
                }
            };

            var result = analyzer.Analyze(klines);

            result.Should().BeTrue();
        }

        // Tweezer Top
        [Fact]
        public void TweezerTop_WhenCandleIsMarobuzu_ShouldReturnTrue()
        {
            // Arrange
            var analyzer = new TweezerTop();
            var klines = new BinanceKline[]
            {
                new BinanceKline
                {
                    High = 100,
                    Close = 100,
                    Open = 0,
                    Low = 0
                },
                new BinanceKline
                {
                    High = 100,
                    Close = 0,
                    Open = 100,
                    Low = 0
                }
            };

            var result = analyzer.Analyze(klines);

            result.Should().BeTrue();
        }

        [Fact]
        public void TweezerTop_WhenCandleIsLong_ShouldReturnTrue()
        {
            // Arrange
            var analyzer = new TweezerTop();
            var klines = new BinanceKline[]
            {
                new BinanceKline
                {
                    High = 100,
                    Close = 80,
                    Open = 20,
                    Low = 0
                },
                new BinanceKline
                {
                    High = 100,
                    Close = 20,
                    Open = 80,
                    Low = 0
                }
            };

            var result = analyzer.Analyze(klines);

            result.Should().BeTrue();
        }

        // One White Soldier
        [Fact]
        public void OneWhiteSoldier_WhenCandleIsMarobuzu_ShouldReturnTrue()
        {
            // Arrange
            var analyzer = new OneWhiteSoldier();
            var klines = new BinanceKline[]
            {
                new BinanceKline
                {
                    High = 100,
                    Close = 0,
                    Open = 100,
                    Low = 0
                },
                new BinanceKline
                {
                    High = 110,
                    Close = 110,
                    Open = 10,
                    Low = 10
                }
            };

            var result = analyzer.Analyze(klines);

            result.Should().BeTrue();
        }

        [Fact]
        public void OneWhiteSoldier_WhenCandleIsLong_ShouldReturnTrue()
        {
            // Arrange
            var analyzer = new OneWhiteSoldier();
            var klines = new BinanceKline[]
            {
                new BinanceKline
                {
                    High = 220,
                    Close = 100,
                    Open = 200,
                    Low = 80
                },
                new BinanceKline
                {
                    High = 230,
                    Close = 210,
                    Open = 110,
                    Low = 90
                }
            };

            var result = analyzer.Analyze(klines);

            result.Should().BeTrue();
        }

        // One Black Crow
        [Fact]
        public void OneBlackCrow_WhenCandleIsMarobuzu_ShouldReturnTrue()
        {
            // Arrange
            var analyzer = new OneBlackCrow();
            var klines = new BinanceKline[]
            {
                new BinanceKline
                {
                    High = 110,
                    Close = 110,
                    Open = 10,
                    Low = 10
                },
                new BinanceKline
                {
                    High = 100,
                    Close = 0,
                    Open = 100,
                    Low = 0
                }
            };

            var result = analyzer.Analyze(klines);

            result.Should().BeTrue();
        }

        [Fact]
        public void OneBlackCrow_WhenCandleIsLong_ShouldReturnTrue()
        {
            // Arrange
            var analyzer = new OneBlackCrow();
            var klines = new BinanceKline[]
            {
                new BinanceKline
                {
                    High = 230,
                    Close = 210,
                    Open = 110,
                    Low = 90
                },
                new BinanceKline
                {
                    High = 220,
                    Close = 100,
                    Open = 200,
                    Low = 80
                }
            };

            var result = analyzer.Analyze(klines);

            result.Should().BeTrue();
        }

        // Bullish Engulfing
        [Fact]
        public void BullishEngulfing_WhenCandleIsLong_ShouldReturnTrue()
        {
            // Arrange
            var analyzer = new BullishEngulfing();
            var klines = new BinanceKline[]
            {
                new BinanceKline
                {
                    High = 1000,
                    Close = 950,
                    Open = 1000,
                    Low = 950
                },
                new BinanceKline
                {
                    High = 950,
                    Close = 900,
                    Open = 950,
                    Low = 900
                },
                new BinanceKline
                {
                    High = 900,
                    Close = 850,
                    Open = 900,
                    Low = 850
                },
                new BinanceKline
                {
                    High = 1000,
                    Close = 950,
                    Open = 850,
                    Low = 800
                }
            };

            var result = analyzer.Analyze(klines);

            result.Should().BeTrue();
        }

        // Bearish Engulfing
        [Fact]
        public void BearishEngulfing_WhenCandleIsLong_ShouldReturnTrue()
        {
            // Arrange
            var analyzer = new BearishEngulfing();
            var klines = new BinanceKline[]
            {
                new BinanceKline
                {
                    High = 900,
                    Close = 900,
                    Open = 850,
                    Low = 850
                },
                new BinanceKline
                {
                    High = 950,
                    Close = 950,
                    Open = 900,
                    Low = 900
                },
                new BinanceKline
                {
                    High = 1000,
                    Close = 1000,
                    Open = 950,
                    Low = 950
                },
                new BinanceKline
                {
                    High = 1050,
                    Close = 900,
                    Open = 1000,
                    Low = 850
                }
            };

            var result = analyzer.Analyze(klines);

            result.Should().BeTrue();
        }

        // Morning Star
        [Fact]
        public void MorningStar_WhenCandleIsLong_ShouldReturnTrue()
        {
            // Arrange
            var analyzer = new MorningStar();
            var klines = new BinanceKline[]
            {
                new BinanceKline
                {
                    High = 1000,
                    Close = 950,
                    Open = 1000,
                    Low = 950
                },
                new BinanceKline
                {
                    High = 950,
                    Close = 900,
                    Open = 950,
                    Low = 900
                },
                new BinanceKline
                {
                    High = 900,
                    Close = 850,
                    Open = 900,
                    Low = 850
                },
                new BinanceKline
                {
                    High = 900,
                    Close = 855,
                    Open = 850,
                    Low = 830
                },
                new BinanceKline
                {
                    High = 920,
                    Close = 890,
                    Open = 855,
                    Low = 850
                }
            };

            var result = analyzer.Analyze(klines);

            result.Should().BeTrue();
        }

        // Evening Star
        [Fact]
        public void EveningStar_WhenCandleIsLong_ShouldReturnTrue()
        {
            // Arrange
            var analyzer = new EveningStar();
            var klines = new BinanceKline[]
            {
                new BinanceKline
                {
                    High = 900,
                    Close = 900,
                    Open = 850,
                    Low = 850
                },
                new BinanceKline
                {
                    High = 950,
                    Close = 950,
                    Open = 900,
                    Low = 900
                },
                new BinanceKline
                {
                    High = 1000,
                    Close = 1000,
                    Open = 950,
                    Low = 950
                },
                new BinanceKline
                {
                    High = 1010,
                    Close = 990,
                    Open = 1000,
                    Low = 900
                },
                new BinanceKline
                {
                    High = 995,
                    Close = 945,
                    Open = 990,
                    Low = 940
                }
            };

            var result = analyzer.Analyze(klines);

            result.Should().BeTrue();
        }
    }
}
